# **Solar System** #

# ** Mini 3D Engine from XML** #
###  ###
The project's goal is to develop a mini scene graph based 3D engine and provide usage examples that show its potential. 

The project is split in four phases:

1. Graphical primitives
2. Geometric Transforms 
3. Curves, Cubic Surfaces and VBOs
4. Normals and Texture Coordinates

This develop this project project it was used the GLUT, GLEW and DevIL lib.